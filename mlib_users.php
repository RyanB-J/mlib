<!-- Ryan Bains-Jordan - Mlib -->

<!DOCTYPE html>
<?php
	session_start();
	include 'mlib_values.php';
	include 'mlib_functions.php';
	include 'mlib_header.php';
	include 'mlib_sidebar.php';
	include 'mlib_footer.php';
?>

<html>
<?php get_meta(); ?>
<body>
	<div class="container">
		<?php
		get_header($_GLOBAL['header']);
		get_navbar($_GLOBAL['main_nav'], "Admin");
		?>
		<div class="row justify-content-sm-center">
			<section class="module col-sm-12 col-lg-8">
				
				<?php
				// Assign variables to form data
				if ( isset( $_SESSION['valid_user'] ) ) {
					$is_admin = true;
				}
				if ( isset( $_POST['first'] ) ) {
					$first = trim( $_POST['first'] );
				}
				if ( isset( $_POST['last'] ) ) {
					$last = trim( $_POST['last'] );
				}
				if ( isset( $_POST['email'] ) ) {
					$email = trim( $_POST['email'] );
				}
				if ( isset( $_POST['submit'] ) ) {
					$submit = $_POST['submit'];
				}
					
				// Database Connection
				$db = db_connection();	
				
				/********** Form not submited **********/
				if ( ! isset( $submit ) and $is_admin ): 
				
				?>
				<h3>Add User</h3>
				<form action="mlib_users.php" method="post">
					<table class="table">
						<thead>
							<tr>
								<th>Field</th>
								<th>Value</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>First Name</td>
								<td><input class="form-control" type="text" name="first" maxlength="50"></td>
							</tr>
							<tr>
								<td>Last Name</td>
								<td><input class="form-control" type="text" name="last" maxlength="50"></td>
							</tr>
							<tr>
								<td>Email</td>
								<td><input class="form-control" type="email" name="email" maxlength="50"></td>
							</tr>
							<tr>
								<td colspan="2"><button type="submit" name="submit" class="btn btn-outline-secondary">Submit</button></td>
							</tr>
						</tbody>
					</table>
				</form>
				<?php 
				$db = null;
				
				/********** Form submitted with empty fields **********/
				elseif ( ( empty( $first ) 
					or empty( $last ) 
					or empty( $email ) ) and $is_admin ):	
				
				?>
				<h3>Add User</h3>
				<form action="mlib_users.php" method="post">
					<table class="table">
						<thead>
							<tr>
								<th>Field</th>
								<th>Value</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>First Name</td>
								<td>
									<input class="form-control" type="text" name="first" value="<?php echo $first; ?>" maxlength="50">
									<?php
									if ( empty( $first ) ) {
										echo '<div class="alert alert-danger">Please enter a first name.</div>';
									}
									?>
								</td>
							</tr>
							<tr>
								<td>Last Name</td>
								<td>
									<input class="form-control" type="text" name="last" value="<?php echo $last; ?>" maxlength="50">
									<?php
									if ( empty( $last ) ) {
										echo '<div class="alert alert-danger">Please enter a last name.</div>';
									}
									?>
								</td>
							</tr>
							<tr>
								<td>Email Address</td>
								<td>
									<input class="form-control" type="email" name="email" value="<?php echo $email; ?>" maxlength="50">
									<?php
									if ( empty( $email ) ) {
										echo '<div class="alert alert-danger">Please enter an email address.</div>';
									}
									?>
								</td>
							</tr>
							<tr>
								<td colspan="2"><button type="submit" name="submit" class="btn btn-outline-secondary">Submit</button></td>
							</tr>
						</tbody>
					</table>
				</form>

				<?php
				$db = null;
				
				/********** Form submitted with populated fields **********/
				elseif ( $is_admin ):
				
				$user_sql = "SELECT COUNT(*) FROM mlib_users WHERE first = '$first' AND last = '$last'";
				$email_sql = "SELECT COUNT(*) FROM mlib_users WHERE email = '$email'";
				$user_result = $db->query($user_sql)->fetch();
				$email_result = $db->query($email_sql)->fetch();
				
				/***** First + Last name or Email fields not unique *****/
				if ( $user_result[0] > 0 or $email_result[0] > 0 ) {
					?>
					<h3>Add User</h3>
					<form action="mlib_users.php" method="post">
						<table class="table">
							<thead>
								<tr>
									<th>Field</th>
									<th>Value</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>First Name</td>
									<td><input class="form-control" type="text" name="first" value="<?php echo $first; ?>" maxlength="50"></td>
								</tr>
								<tr>
									<td>Last Name</td>
									<td><input class="form-control" type="text" name="last" value="<?php echo $last; ?>" maxlength="50"></td>
								</tr>
								<tr>
									<td>Email</td>
									<td>
										<input class="form-control" type="email" name="email" value="<?php echo $email; ?>" maxlength="50">
										<?php
										if ( $email_result[0] > 0 ) {
											echo '<div class="alert alert-danger">Email already in use.</div>';
										}
									?>
									</td>
								</tr>
								<?php
								if ( $user_result[0] > 0 ) {
									echo '<tr><td colspan="2"><div class="alert alert-danger">The name "' . $first . ' ' . $last . '" is already in use.</div></td></tr>';
								}
								?>
								<tr>
									<td colspan="2"><button type="submit" name="submit" class="btn btn-outline-secondary">Submit</button></td>
								</tr>
							</tbody>
						</table>
					</form>
					<?php
						
					$db = null;
				}
				
				/***** Name and Email fields unique *****/
				else {
					
					// Add data to database
					$sql = "INSERT INTO mlib_users VALUES ( NULL, '$first', '$last', '$email', '', '' )";
					$db->exec($sql);
					
					?>
					<h3>User Added</h3>
					<table class="table">
						<thead>
							<tr>
								<th>First Name</th>
								<th>Last Name</th>
								<th>Email</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><?php echo $first; ?></td>
								<td><?php echo $last; ?></td>
								<td><?php echo $email; ?></td>
							</tr>
						</tbody>
					</table>
					<?php
						
					$db = null;
					
					// Button to enter another User
					?>
					<form action="mlib_users.php">
						<button role="button" class="btn btn-outline-secondary">Add Another User</button>
					</form>
					<?php
				}
				
				/***** If user is not an admin *****/
				else:
				
				we_are_not_admin();
				$db = null;
				
				endif;
				?>

			</section>
		</div>
	</div>
	<?php get_footer() ?>
</body>
</html>