<!-- Ryan Bains-Jordan - Mlib -->

<!DOCTYPE html>
<?php
	session_start();
	include 'mlib_values.php';
	include 'mlib_functions.php';
	include 'mlib_header.php';
	include 'mlib_footer.php';
?>

<html>
<?php get_meta(); ?>
<body>
	<div class="container">
		<?php
		get_header($_GLOBAL['header']);
		
		// If logged in, log out
		if ( isset( $_SESSION['valid_user'] ) ) {
			
			// Free session variables and remove them
			unset( $_SESSION['valid_user'] );
			session_destroy();
			
			include 'mlib_sidebar.php';
			get_navbar($_GLOBAL['main_nav'], "Logout");
			?>
			<div class="row justify-content-sm-center">
				<section class="module col-sm-12 col-lg-8">
					<div class="alert alert-primary">You are logged out.</div>
				</section>
			</div>
			<?php
				
		} else {
			include 'mlib_sidebar.php';
			get_navbar($_GLOBAL['main_nav'], "Logout");
			?>
			<div class="row justify-content-sm-center">
				<section class="module col-sm-12 col-lg-8">
					<div class="alert alert-primary">You are already logged out.</div>
				</section>
			</div>
			<?php
		}
		
		?>
	</div>
	<?php get_footer() ?>
</body>
</html>