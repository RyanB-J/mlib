<?php
/*
* MLib Footer
* Copyright, Name, and required scripts (jQuery, Bootstrap, Popper)	
*/

function get_footer() {
	?>
	<footer class="footer">
		<p class="lead">CSCI59P - Ryan Bains-Jordan</p>	
	</footer>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
	<script src="src/bootstrap.js"></script>
	<?php
}

?>
