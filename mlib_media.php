<!-- Ryan Bains-Jordan - Mlib -->

<!DOCTYPE html>
<?php
	session_start();
	include 'mlib_values.php';
	include 'mlib_functions.php';
	include 'mlib_header.php';
	include 'mlib_sidebar.php';
	include 'mlib_footer.php';
?>

<html>
<?php get_meta(); ?>
<body>
	<div class="container">
		<?php
		get_header($_GLOBAL['header']);
		get_navbar($_GLOBAL['main_nav'], "Admin");
		?>
		<div class="row justify-content-sm-center">
			<section class="module col-sm-12 col-lg-8">
				
				<?php
				// Assign variables to form data
				if ( isset( $_SESSION['valid_user'] ) ) {
					$is_admin = true;
				}
				if ( isset( $_POST['title'] ) ) {
					$title = trim( $_POST['title'] );
				}
				if ( isset( $_POST['author'] ) ) {
					$author = trim( $_POST['author'] );
				}
				if ( isset( $_POST['description'] ) ) {
					$description = trim( $_POST['description'] );
				}
				if ( isset( $_POST['type'] ) ) {
					$type = trim( $_POST['type'] );
				}
				if ( isset( $_POST['submit'] ) ) {
					$submit = $_POST['submit'];
				}
					
				// Database Connection
				$db = db_connection();	
				
				/********** Form not submited **********/
				if ( ! isset( $submit ) and $is_admin ): 
				
				?>
				<h3>Add Media</h3>
				<form action="mlib_media.php" method="post">
					<table class="table">
						<thead>
							<tr>
								<th>Field</th>
								<th>Value</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Title</td>
								<td><input class="form-control" type="text" name="title" maxlength="50"></td>
							</tr>
							<tr>
								<td>Author/Director</td>
								<td><input class="form-control" type="text" name="author" maxlength="50"></td>
							</tr>
							<tr>
								<td>Description</td>
								<td><input class="form-control" type="text" name="description" maxlength="100"></td>
							</tr>
							<tr>
								<td>Type</td>
								<td>
									<select class="form-control" name="type">
										<?php
										$types = $db->query("SELECT * FROM mlib_types");
										foreach ( $types as $row ) {
											echo '<option value="' . $row['type'] . '">' . $row['type'] . '</option>';
										}
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td colspan="2"><button type="submit" name="submit" class="btn btn-outline-secondary">Submit</button></td>
							</tr>
						</tbody>
					</table>
				</form>
				<?php 
				$db = null;
				
				/********** Form submitted with empty fields **********/
				elseif ( ( empty( $title ) 
					or empty( $author ) 
					or empty( $description ) ) and $is_admin ):	
				
				?>
				<h3>Add Media</h3>
				<form action="mlib_media.php" method="post">
					<table class="table">
						<thead>
							<tr>
								<th>Field</th>
								<th>Value</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Title</td>
								<td>
									<input class="form-control" type="text" name="title" value="<?php echo $title; ?>" maxlength="50">
									<?php
									if ( empty( $title ) ) {
										echo '<div class="alert alert-danger">Media must have a title.</div>';
									}
									?>
								</td>
							</tr>
							<tr>
								<td>Author/Director</td>
								<td>
									<input class="form-control" type="text" name="author" value="<?php echo $author; ?>" maxlength="50">
									<?php
									if ( empty( $author ) ) {
										echo '<div class="alert alert-danger">Media must have an author.</div>';
									}
									?>
								</td>
							</tr>
							<tr>
								<td>Description</td>
								<td>
									<input class="form-control" type="text" name="description" value="<?php echo $description; ?>" maxlength="100">
									<?php
									if ( empty( $description ) ) {
										echo '<div class="alert alert-danger">Media must have a description.</div>';
									}
									?>
								</td>
							</tr>
							<tr>
								<td>Type</td>
								<td>
									<select class="form-control" name="type" value="<?php echo $type; ?>">
										<?php
										$types = $db->query("SELECT * FROM mlib_types");
										foreach ( $types as $row ) {
											echo '<option value="' . $row['type'] . '">' . $row['type'] . '</option>';
										}
										?>
									</select>
								</td>
							</tr>
							<tr>
								<td colspan="2"><button type="submit" name="submit" class="btn btn-outline-secondary">Submit</button></td>
							</tr>
						</tbody>
					</table>
				</form>

				<?php
				$db = null;
				
				/********** Form submitted with populated fields **********/
				elseif ( $is_admin ):
				
				$sql = "SELECT COUNT(*) FROM media WHERE title = '$title' AND status = 'active'";
				$result = $db->query($sql)->fetch();
				
				/***** Title field not unique *****/
				if ( $result[0] > 0 ) {
					?>
					<h3>Add Media</h3>
					<form action="mlib_media.php" method="post">
						<table class="table">
							<thead>
								<tr>
									<th>Field</th>
									<th>Value</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Title</td>
									<td>
										<input class="form-control" type="text" name="title" value="<?php echo $title; ?>" maxlength="50">
										<?php echo '<div class="alert alert-danger">"' . $title . '" already in use. Use a different name.'; ?>
									</td>
								</tr>
								<tr>
									<td>Author/Director</td>
									<td><input class="form-control" type="text" name="author" value="<?php echo $author; ?>" maxlength="50"></td>
								</tr>
								<tr>
									<td>Description</td>
									<td><input class="form-control" type="text" name="description" value="<?php echo $description; ?>" maxlength="100"></td>
								</tr>
								<tr>
									<td>Type</td>
									<td>
										<select class="form-control" name="type" value="<?php echo $type; ?>">
											<?php
											$types = $db->query("SELECT * FROM mlib_types");
											foreach ( $types as $row ) {
												echo '<option value="' . $row['type'] . '">' . $row['type'] . '</option>';
											}
											?>
										</select>
									</td>
								</tr>
								<tr>
									<td colspan="2"><button type="submit" name="submit" class="btn btn-outline-secondary">Submit</button></td>
								</tr>
							</tbody>
						</table>
					</form>
					<?php
						
					$db = null;
				}
				
				/***** Title field unique *****/
				else {
					
					// Add data to database
					$sql = "INSERT INTO media VALUES ( NULL, '$title', '$author', '$description', '$type', 0, 'active', '' )";
					$db->exec($sql);
					
					?>
					<h3>Media Added</h3>
					<table class="table">
						<thead>
							<tr>
								<th>Title</th>
								<th>Author/Director</th>
								<th>Description</th>
								<th>Type</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><?php echo $title; ?></td>
								<td><?php echo $author; ?></td>
								<td><?php echo $description; ?></td>
								<td><?php echo $type; ?></td>
							</tr>
						</tbody>
					</table>
					<?php
						
					$db = null;
					
					// Button to enter another Media
					?>
					<form action="mlib_media.php">
						<button role="button" class="btn btn-outline-secondary">Add Another Item</button>
					</form>
					<?php
				}
				
				/***** Is not an admin *****/
				else:
				
				we_are_not_admin();
				$db = null;
				
				endif;
				?>

			</section>
		</div>
	</div>
	<?php get_footer() ?>
</body>
</html>