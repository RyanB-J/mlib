<!-- Ryan Bains-Jordan - Mlib -->

<!DOCTYPE html>
<?php
	session_start();
	include 'mlib_values.php';
	include 'mlib_functions.php';
	include 'mlib_header.php';
	include 'mlib_sidebar.php';
	include 'mlib_footer.php';
?>

<html>
<?php get_meta(); ?>
<body>
	<div class="container">
		<?php 
		get_header($_GLOBAL['header']);
		get_navbar($_GLOBAL['main_nav'], "Release Media");
		?>
		<div class="row justify-content-sm-center">
			<section class="module col-sm-12 col-lg-8">
				<?php
				
				// Set POST variables if they exist
				if (isset($_POST['id'])) {
					$id = $_POST['id'];
				}
				if (isset($_POST['submit'])) {
					$submit = $_POST['submit'];
				}
				
				// Database Connection
				$db = db_connection();
				
				// Display everything available in media table
				echo "<h3>Release Media</h3>";
				
				/********** If form has not been submitted **********/	
				if (!isset($submit)):
				?>
				<!-- Form has not been submitted -->
				<form action="mlib_release.php" method="post">
					<table class="table table-overflow">
						<thead>
							<tr>
								<th class="check-col"></th>
								<th>Media</th>
								<th>Author/Director</th>
								<th>Description</th>
								<th>Reserved By</th>
								<th>Due By</th>
							</tr>
						</thead>
						<tbody>
							<?php
							// Create a list of all available media entries
							$result = $db->query("SELECT media.*,mlib_users.first,mlib_users.last FROM media INNER JOIN mlib_users ON media.user_id = mlib_users.id ORDER BY title");
							foreach ($result as $row) {
								echo '<tr>';
								echo '<td><input type="checkbox" class="form-control" name="id[]" value="'.$row['id'].'"></td>';
								echo '<td>'.$row['title'].'</td>';
								echo '<td>'.$row['author'].'</td>';
								echo '<td>'.$row['description'].'</td>';
								echo '<td>'.$row['first'].' '.$row['last'].'</td>';
								echo '<td>'.$row['date_in'].'</td>';
								echo '</tr>';
							}
							?>
						</tbody>
					</table>
					<input type="submit" class="btn btn-outline-secondary" name="submit" value="Release">
				</form>
						
				<?php
				$db = null;
				
				/********** If form has been submitted with errors **********/
				elseif (isset($submit) and count($id) < 1):
				?>
				<!-- Form has been submitted -->
				<form action="mlib_release.php" method="post">
					<table class="table">
						<thead>
							<tr>
								<th class="check-col"></th>
								<th>Media</th>
								<th>Author/Director</th>
								<th>Description</th>
								<th>Type</th>
								<th>Due By</th>
							</tr>
						</thead>
						<tbody>
							<?php
							// Create a list of all available media entries
							$result = $db->query("SELECT media.*,mlib_users.first,mlib_users.last FROM media INNER JOIN mlib_users ON media.user_id = mlib_users.id ORDER BY title");
							foreach ($result as $row) {
								echo '<tr>';
								echo '<td><input type="checkbox" class="form-control" name="id[]" value="'.$row['id'].'"></td>';
								echo '<td>'.$row['title'].'</td>';
								echo '<td>'.$row['author'].'</td>';
								echo '<td>'.$row['description'].'</td>';
								echo '<td>'.$row['first'].' '.$row['last'].'</td>';
								echo '<td>'.$row['date_in'].'</td>';
								echo '</tr>';
							}
							?>
						</tbody>
					</table>
					<?php
					if (count($id) < 1) {
						echo '<div class="alert alert-danger">Please select one or more items to reserve.</div>';
					}	
					?>
					<input type="submit" class="btn btn-outline-secondary" name="submit" value="Release">
				</form>
						
				<?php
				$db = null;

				/********** If form has been submitted with no errors **********/
				else:
				
				// Update media entries
				for ($i = 0; $i < count($id); $i++) {
					$db->exec("UPDATE media SET user_id = 0, date_in = '0000-00-00' WHERE id = $id[$i]");
				}
				
				echo '<div class="alert alert-success">The following media has been released:</div>';
				?>
				<table class="table">
					<thead>
						<tr>
							<th>Title</th>
							<th>Author</th>
							<th>Description</th>
							<th>Type</th>
						</tr>
					</thead>
					<tbody>
						<?php
						for ($i = 0; $i < count($id); $i++) {
							
							// Grab each row from the database where id = $id
							$sql = "SELECT * FROM media WHERE id = $id[$i]";
							$row = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
							
							// Display a new row for each entry
							echo '<tr>';
							echo '<td>'.$row['title'].'</td>';
							echo '<td>'.$row['author'].'</td>';
							echo '<td>'.$row['description'].'</td>';
							echo '<td>'.$row['type'].'</td>';
							echo '</tr>';
						}
						?>
					</tbody>
				</table>
				
				<a href="mlib_release.php" class="btn btn-outline-secondary" role="button">Release More</a>
				
				<?php
				$db = null;
				
				endif;
												
				?>			
			</section>
		</div>
	</div>
	<?php get_footer() ?>
</body>
</html>