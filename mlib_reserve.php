<!-- Ryan Bains-Jordan - Mlib -->

<!DOCTYPE html>
<?php
	session_start();
	include 'mlib_values.php';
	include 'mlib_functions.php';
	include 'mlib_header.php';
	include 'mlib_sidebar.php';
	include 'mlib_footer.php';
?>

<html>
<?php get_meta(); ?>
<body>
	<div class="container">
		<?php 
		get_header($_GLOBAL['header']);
		get_navbar($_GLOBAL['main_nav'], "Reserve Media");
		?>
		<div class="row justify-content-sm-center">
			<section class="module col-sm-12 col-lg-8">
				<?php
				
				// Set POST variables if they exist
				if (isset($_POST['user'])) {
					$user_id = $_POST['user'];
				}
				if (isset($_POST['date'])) {
					$date = convert_date($_POST['date']);
				}
				if (isset($_POST['id'])) {
					$id = $_POST['id'];
				}
				if (isset($_POST['submit'])) {
					$submit = $_POST['submit'];
				}
				
				// Database Connection
				$db = db_connection();
				
				// Display everything available in media table
				echo "<h3>Reserve Media</h3><hr>";
				
				// Get the date next week
				$result = $db->query('SELECT CURDATE() + INTERVAL 1 WEEK')->fetch();
				$next_week = $result[0];
				
				/********** If form has not been submitted **********/	
				if (!isset($submit)):
				?>
				<form action="mlib_reserve.php" method="post">
					<div class="form-group">
						<label for="user">Checked Out to:</label>
						<select class="form-control" name="user">
							<?php
							// Display options for all of the Users
							$sql = "SElECT * FROM mlib_users";
							$result = $db->query($sql);
							foreach ($result as $row) {
								echo '<option value="'.$row['id'].'">'.$row['first'].' '.$row['last'].'</option>';
							}	
							?>
						</select>
					</div>
					<div class="form-group">
						<label for="user">Reserve Until:</label>
						<input type="date" class="form-control" id="date" name="date" value="<?php echo $next_week; ?>">
					</div>
					<table class="table">
						<thead>
							<tr>
								<th class="check-col"></th>
								<th>Media</th>
								<th>Author/Director</th>
								<th>Description</th>
								<th>Type</th>
							</tr>
						</thead>
						<tbody>
							<?php
							// Create a list of all available media entries
							$result = $db->query("SELECT * FROM media WHERE `status` = 'active' AND user_id = 0 ORDER BY title");
							foreach ($result as $row) {
								echo '<tr>';
								echo '<td><input type="checkbox" class="form-control" name="id[]" value="'.$row['id'].'"></td>';
								echo '<td>'.$row['title'].'</td>';
								echo '<td>'.$row['author'].'</td>';
								echo '<td>'.$row['description'].'</td>';
								echo '<td>'.$row['type'].'</td>';
								echo '</tr>';
							}
							?>
						</tbody>
					</table>
					<input type="submit" class="btn btn-outline-secondary" name="submit" value="Reserve">
				</form>
						
				<?php
				$db = null;
				
				/********** If form has been submitted with errors **********/
				elseif (isset($submit) and (check_date($date) != null or count($id) < 1)):
				?>
				<form action="mlib_reserve.php" method="post">
					<div class="form-group">
						<label for="user">Checked Out to:</label>
						<?php
						
						?>
						<select class="form-control" name="user">
							<?php
							// Display options for all of the Users
							$sql = "SElECT * FROM mlib_users";
							$result = $db->query($sql);
							foreach ($result as $row) {
								echo '<option value="'.$row['id'].'"'.
									($row['id'] == $user_id ? ' selected>' : '>')
									.$row['first'].' '.$row['last'].'</option>';
							}	
							?>
						</select> 
					</div>
					<div class="form-group">
						<label for="user">Reserve Until:</label>
						<input type="date" class="form-control" id="date" name="date" value="<?php echo $date; ?>">
						<?php
						if (check_date($date) != null) {
							echo '<div class="alert alert-danger">'.check_date($date).'</div>';
						}
						?>
					</div>
					<table class="table">
						<thead>
							<tr>
								<th class="check-col"></th>
								<th>Media</th>
								<th>Author/Director</th>
								<th>Description</th>
								<th>Type</th>
							</tr>
						</thead>
						<tbody>
							<?php
							// Create a list of all available media entries
							$result = $db->query("SELECT * FROM media WHERE `status` = 'active' AND user_id = 0 ORDER BY title");
							foreach ($result as $row) {
								echo '<tr>';
								echo '<td><input type="checkbox" class="form-control" name="id[]" value="'.$row['id'].'"></td>';
								echo '<td>'.$row['title'].'</td>';
								echo '<td>'.$row['author'].'</td>';
								echo '<td>'.$row['description'].'</td>';
								echo '<td>'.$row['type'].'</td>';
								echo '</tr>';
							}
							?>
						</tbody>
					</table>
					<?php
					if (count($id) < 1) {
						echo '<div class="alert alert-danger">Please select one or more items to reserve.</div>';
					}	
					?>
					<input type="submit" class="btn btn-outline-secondary" name="submit" value="Reserve">
				</form>
						
				<?php
				$db = null;

				/********** If form has been submitted with no errors **********/
				else:
				
				// Update media entries
				for ($i = 0; $i < count($id); $i++) {
					$db->exec("UPDATE media SET user_id = $user_id, date_in = '$date' WHERE id = $id[$i]");
				}
				
				echo '<div class="alert alert-success">The following media has been reserved:</div>';
				?>
				<table class="table">
					<thead>
						<tr>
							<th>Title</th>
							<th>Author</th>
							<th>Description</th>
							<th>Type</th>
							<th>Due By</th>
						</tr>
					</thead>
					<tbody>
						<?php
						for ($i = 0; $i < count($id); $i++) {
							
							// Grab each row from the database where id = $id
							$sql = "SELECT * FROM media WHERE id = $id[$i]";
							$row = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
							
							// Display a new row for each entry
							echo '<tr>';
							echo '<td>'.$row['title'].'</td>';
							echo '<td>'.$row['author'].'</td>';
							echo '<td>'.$row['description'].'</td>';
							echo '<td>'.$row['type'].'</td>';
							echo '<td>'.$row['date_in'].'</td>';
							echo '</tr>';
						}
						?>
					</tbody>
				</table>
				
				<a href="mlib_reserve.php" class="btn btn-outline-secondary" role="button">Reserve More</a>
				
				<?php
				$db = null;
				
				endif;
												
				?>			
			</section>
		</div>
	</div>
	<?php get_footer() ?>
</body>
</html>