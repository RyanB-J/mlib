<!-- Ryan Bains-Jordan - Mlib -->

<!DOCTYPE html>
<?php
	session_start();
	include 'mlib_values.php';
	include 'mlib_functions.php';
	include 'mlib_header.php';
	include 'mlib_sidebar.php';
	include 'mlib_footer.php';
?>

<html>
<?php get_meta(); ?>
<body>
	<div class="container">
		<?php
		get_header($_GLOBAL['header']);
		get_navbar($_GLOBAL['main_nav'], "Admin");
		?>
		
		<div class="row justify-content-sm-center">
			<section class="module col-sm-12 col-lg-8">
			
			<?php
			if ( isset( $_SESSION['valid_user'] ) )	 {
			?>
			<h3>Add Media</h3>
			<hr>
			<form action="mlib_process_media.php" method="post" enctype="multipart/form-data">
				<div class="form-group">
					<input type="hidden" name="MAX_FILE_SIZE" value="1000">
					<label for="file-upload">Upload a file for processing</label>
					<input type="file" class="form-control-file" name="userfile">
					<hr>
					<input type="submit" class="btn btn-outline-secondary" name="submit-upload" value="Process">
				</div>
			</form>
			<?php
			} else {
				we_are_not_admin();
			}
			?>
			
			</section>
		</div>
		
	</div>
	<?php get_footer(); ?>
</body>
</html>