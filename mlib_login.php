<!-- Ryan Bains-Jordan - Mlib -->

<!DOCTYPE html>
<?php
	session_start();
	include 'mlib_values.php';
	include 'mlib_functions.php';
	include 'mlib_header.php';
	include 'mlib_footer.php';
?>

<html>
<?php get_meta(); ?>
<body>
	<div class="container">
				
		<?php
			
		// Assign variables to form data
		if ( isset( $_SESSION['valid_user'] ) ) {
			$logged_in = true;
		}
		if ( isset( $_POST['login'] ) ) {
			$login = trim( $_POST['login'] );
		}
		if ( isset( $_POST['password'] ) ) {
			$password = trim( $_POST['password'] );
		}
		if ( isset( $_POST['submit'] ) ) {
			$submit = $_POST['submit'];
		}
		
		// Database Connection
		$db = db_connection();	
		
		/********** Form not submited **********/
		if ( ! isset( $submit ) and ! $logged_in ):
		
		include 'mlib_sidebar.php';
		get_header($_GLOBAL['header']);
		get_navbar($_GLOBAL['main_nav'], "Login");
		?>
		<div class="row justify-content-sm-center">
			<section class="module col-sm-12 col-lg-8">
				<h3>Login</h3>
				<form action="mlib_login.php" method="post">
					<table class="table">
						<thead>
							<tr>
								<th>Field</th>
								<th>Value</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Username</td>
								<td><input class="form-control" type="text" name="login" maxlength="50"></td>
							</tr>
							<tr>
								<td>Password</td>
								<td><input class="form-control" type="password" name="password" maxlength="50"></td>
							</tr>
							<tr>
								<td colspan="2"><button type="submit" name="submit" class="btn btn-outline-secondary">Submit</button></td>
							</tr>
						</tbody>
					</table>
				</form>
			</section>
		</div>
		<?php 
		$db = null;
		
		/********** Form submitted with empty fields **********/
		elseif ( empty( $login ) 
			|| empty( $password ) and ! $logged_in ):
		
		include 'mlib_sidebar.php';
		get_header($_GLOBAL['header']);
		get_navbar($_GLOBAL['main_nav'], "Login");
		?>
		
		<div class="row justify-content-sm-center">
			<section class="module col-sm-12 col-lg-8">
				<h3>Login</h3>
				<form action="mlib_login.php" method="post">
					<table class="table">
						<thead>
							<tr>
								<th>Field</th>
								<th>Value</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Username</td>
								<td>
									<input class="form-control" type="text" name="login" value="<?php echo $login; ?>" maxlength="50">
									<?php
									if ( empty( $login ) ) {
										echo '<div class="alert alert-danger">Please enter a username.</div>';
									}
									?>
								</td>
							</tr>
							<tr>
								<td>Password</td>
								<td>
									<input class="form-control" type="password" name="password" value="<?php echo $password; ?>" maxlength="50">
									<?php
									if ( empty( $password ) ) {
										echo '<div class="alert alert-danger">Please enter a password.</div>';
									}
									?>
								</td>
							</tr>
							<tr>
								<td colspan="2"><button type="submit" name="submit" class="btn btn-outline-secondary">Submit</button></td>
							</tr>
						</tbody>
					</table>
				</form>
			</section>
		</div>
		
		<?php
		$db = null;
		
		/********** Form submitted with populated fields **********/
		elseif ( ! $logged_in ):
		
		// Hash password
		$hpassword = sha1( $password );
		
		// Populate Session variable if data is correct
		$sql = "SELECT count(*) FROM mlib_users WHERE login = '$login' AND `password` = '$hpassword'";
		$result = $db->query($sql)->fetch();
		if ( $result[0] == 1 ) {
			
			$_SESSION['valid_user'] = $login;
			
			include 'mlib_sidebar.php';
			get_header($_GLOBAL['header']);
			get_navbar($_GLOBAL['main_nav'], "Login");
			
			?>
			<div class="row justify-content-sm-center">
				<section class="module col-sm-12 col-lg-8">
					<h3>Login</h3>
					<div class="alert alert-primary">You are logged in as <?php echo $login; ?></div>
				</section>
			</div>
			<?php
		}
		
		// If data is not correct
		else {
			
			include 'mlib_sidebar.php';
			get_header($_GLOBAL['header']);
			get_navbar($_GLOBAL['main_nav'], "Login");

			?>
			<div class="row justify-content-sm-center">
				<section class="module col-sm-12 col-lg-8">
					<h3>Login</h3>
					<div class="alert alert-danger">Either your login or password is incorrect.</div>
					<form action="mlib_login.php" method="post">
						<table class="table">
							<thead>
								<tr>
									<th>Field</th>
									<th>Value</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Username</td>
									<td><input class="form-control" type="text" name="login" value="<?php echo $login; ?>" maxlength="50"></td>
								</tr>
								<tr>
									<td>Password</td>
									<td><input class="form-control" type="password" name="password" value="<?php echo $password; ?>" maxlength="50"></td>
								</tr>
								<tr>
									<td colspan="2"><button type="submit" name="submit" class="btn btn-outline-secondary">Submit</button></td>
								</tr>
							</tbody>
						</table>
					</form>
				</section>
			</div>
			<?php
				
			$db = null;	
		}
		
		else:
		
		include 'mlib_sidebar.php';
		get_header($_GLOBAL['header']);
		get_navbar($_GLOBAL['main_nav'], "Login");
		
		?>
		<div class="row justify-content-sm-center">
			<section class="module col-sm-12 col-lg-8">
				<h3>Login</h3>
				<div class="alert alert-primary">You are already logged in as <?php echo $login; ?></div>
			</section>
		</div>
		<?php
		
		$db = null;
		
		endif;
		?>
				
	</div>
	<?php get_footer() ?>
</body>
</html>