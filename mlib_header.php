<?php
/*
* MLib Header
* Header Section Constructor
*/

function get_meta() {
	?>
	<head>
		<title>MLib</title>
		<meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <meta name="description" content="Check out books and movies with Mlib.com">
	    <link rel="stylesheet" href="src/bootstrap_type.css">
	    <link rel="stylesheet" href="src/bootstrap_grid.css">
	    <link rel="stylesheet" href="src/bootstrap_navs.css">
	    <link rel="stylesheet" href="src/bootstrap_forms.css">
	    <link rel="stylesheet" href="src/bootstrap_tables.css">
	    <link rel="stylesheet" href="src/bootstrap_buttons.css">
	    <link rel="stylesheet" href="src/bootstrap_alert.css">
		<link rel="stylesheet" href="mlib.css">
	</head>
	<?php
}
	
function get_header( $array ) {
	?>
	<div class="row">
		<header class="header">
			<h1 class="display-4">MLib - Media Library</h1>
			<ul class="list-inline">
				<li class="list-inline-item">Homework 7-9 and 11-12</li>
				<li class="list-inline-item">|</li>
				<li class="list-inline-item">Ryan Bains-Jordan</li>
				<li class="list-inline-item">|</li>
				<li class="list-inline-item">Week 7-12</li>
			</ul>
		</header>
	</div>
	<?php
}
?>