<!-- Ryan Bains-Jordan - Mlib -->

<!DOCTYPE html>
<?php
	session_start();
	include 'mlib_values.php';
	include 'mlib_functions.php';
	include 'mlib_header.php';
	include 'mlib_sidebar.php';
	include 'mlib_footer.php';
?>

<html>
<?php get_meta(); ?>
<body>
	<div class="container">
		<?php
		get_header($_GLOBAL['header']);
		get_navbar($_GLOBAL['main_nav'], "Admin");
		?>
		
		<div class="row justify-content-sm-center">
			<section class="module col-sm-12 col-lg-8">
			
			<?php
				
			/***** If "Add to Database" has not been clicked *****/
			if ( ! isset( $_POST['save'] ) and isset( $_SESSION['valid_user'] ) ):
						
			// If there is a problem with the upload
			$message = '<div class="alert ';
			$tryagain = '<a class="btn btn-outline-danger" role="button" href="mlib_upload.php">Try Again</a>';
			if ( $_FILES['userfile']['error'] ) {
				
				// Name the Problem
				switch ( $_FILES['userfile']['error'] ) {	
					case 1: $message .= 'alert-danger">Problem: File exceeded upload_max_filesize.</div>';
						echo $message.$tryagain;
						break;
					case 2: $message .= 'alert-danger">Problem: File exceded max_file_size.</div>';
						echo $message.$tryagain;
						break;
					case 3: $message .= 'alert-danger">Problem: File only partially uploaded.</div>';
						echo $message.$tryagain;
						break;
					case 4: $message .= 'alert-danger">Problem: No File Uploaded.</div>';
						echo $message.$tryagain;
						break;
					case 6: $message .= 'alert-danger">Problem: Cannot upload file: No temp directory specified.</div>';
						echo $message.$tryagain;
						break;
					case 7: $message .= 'alert-danger">Problem: Upload failed: Cannot write to disk.</div>';
						echo $message.$tryagain;
						break;
				}
				exit;
			}
			
			// Does the file have the right MIME type
			if ($_FILES['userfile']['type'] != 'text/plain') {
				$message .= 'alert-danger">Problem: File is not plain text.</div>';
				echo $message.$tryagain;
				exit;
			}
			
			// Put the file where we'd like it
			$upfile = './uploads/'.$_FILES['userfile']['name'];
			if (is_uploaded_file($_FILES['userfile']['tmp_name'])) {
				if (!move_uploaded_file($_FILES['userfile']['tmp_name'], $upfile)){
			    	$message .= 'alert-danger">Problem: Could not move file to destination directory.</div>';
			    	echo $message.$tryagain;
					exit;
				}
			} else {
				$message .= 'alert-danger">Problem: Possible file upload attack. Filename: ';
				$message .= $_FILES['userfile']['name'] . '</div>';
				echo $message.$tryagain;
				exit;
			}
			
			$message .= 'alert-success">File uploaded successfully.</div>';
			echo $message;
			echo '<h3>' . $_FILES['userfile']['name'] . '</h3>';
			
			// Display the file contents
			?>
			<table class="table">
				<thead>
					<tr>
						<th></th>
						<th>Title</th>
						<th>Author</th>
						<th>Type</th>
						<th>Description</th>
					</tr>
				</thead>
				<tbody>
				<?php
				
				$fp = fopen($upfile, 'r');
				
				if (!$fp) {
				  echo "<p>Could not open $upfile.";
				  exit;
				}
				
				// Connect to database
				$db = db_connection();
				
				// Counter for media entries
				$count = 0;
				
				// Set an error boolean to ensure bad data isnt uploaded
				$error = false;
				
				// Read data from the file and process it a line at a time.
				while (!feof($fp)) {
					$line = trim(fgets($fp));
					if (empty($line)) continue;
					$line_array = explode(',', $line);
					$title = trim($line_array[0]);
					$author = trim($line_array[1]);
					$type = strtolower(trim($line_array[2]));
					$description = trim($line_array[3]);
					
					?>
					<tr>
						<td><?php echo ++$count; ?></td>
						<td>
							<?php
							echo $title;
							
							// Check Database for like titles
							$sql = "SELECT COUNT(*) FROM media WHERE title = \"$title\" AND status = \"active\"";
							$result = $db->query($sql)->fetch();
							
							// Check if title field is empty or if it already exists
							if (empty($title)) {
								echo '<div class="alert alert-danger">The title field must have a name.</div>';
								$error = true;
							} elseif ($result[0] > 0) {
								echo '<br><div class="alert alert-danger">"'.$title.'" is already in use. Use a different name.</div>';
								$error = true;
							}
							
							?>
						</td>
						<td>
							<?php
							echo $author;
							
							// Check if author field is empty
							if (empty($author)) {
								echo '<div class="alert alert-danger">The author field must have a name.</div>';
								$error = true;
							}
							?>
						</td>
						<td>
							<?php
							
							echo $type;
							
							// Check Database to ensure type is available
							$sql = "SELECT COUNT(*) FROM mlib_types WHERE type = \"$type\" AND status = \"active\"";
							$result = $db->query($sql)->fetch();
							
							// Check if type field is empty or if it is not defined
							if (empty($type)) {
								echo '<div class="alert alert-danger">The type field must have a type.</div>';
								$error = true;
							} elseif ($result[0] == 0) {
								echo '<br><div class="alert alert-danger">"'.$type.'" is not defined. Use a valid type.</div>';
								$error = true;
							}
							
							?>
						</td>
						<td>
							<?php
							echo $description;
							
							// Check if description field is empty
							if (empty($description)) {
								echo '<div class="alert alert-danger">The description field must have a description.</div>';
								$error = true;
							}
							?>
						</td>
					</tr>
					<?php
				}
				fclose($fp);
				$db = null;
				
				?>
				</tbody>
			</table>
			<?php
			
			// Go back to previous page if there are errors
			if ($error == true) {
				echo '<em>Please fix <span style="color:red;">red</span> errors and try uploading again.</em>';
				echo '<br>'.$tryagain;
				
				// Delete uploaded file
				unlink($upfile);
				
			// Otherwise show a button for adding entries to the database
			} else {
				?>
				<form action="mlib_process_media.php" method="post">
					<input class="btn btn-outline-success" type="submit" name="save" value="Save to Database">
					<input type="hidden" name="file" value="<?php echo $upfile; ?>">
				</form>
				<?php
			}
			
			/***** If "Add to database" has been clicked *****/
			elseif ( isset( $_SESSION['valid_user'] ) ):
			
			$upfile = $_POST['file'];
			$fp = fopen($upfile, 'r');
				
			if (!$fp) {
				echo "<p>Could not open $upfile.";
				exit;
			}
			
			// Connect to Database
			$db = db_connection();
			
			// Read data from the file and process it a line at a time.
			while (!feof($fp)) {
				$line = trim(fgets($fp));
				if (empty($line)) continue;
				$line_array = explode(',', $line);
				$title = trim($line_array[0]);
				$author = trim($line_array[1]);
				$type = strtolower(trim($line_array[2]));
				$description = trim($line_array[3]);
				
				// Insert into database
				$sql = "INSERT INTO media VALUES ( NULL, '$title', '$author', '$description', '$type', 0, 'active', '' )";
				if ($db->exec($sql)) {
					echo '<span style="color:green;">Successfully added "'.$title.'" to database.</span>';
				} else {
					echo '<span style="color:red;">"'.$title.'" was not added to the database.</span>';
				}
			}
			
			fclose($fp);
			$db = null;
			
			// Delete uploaded file
			unlink($upfile);
			
			/***** If user is not an admin *****/
			else:
			
			we_are_not_admin();
			
			endif;
			
			?>
				
			</section>
		</div>
		
	</div>
	<?php get_footer(); ?>
</body>
</html>