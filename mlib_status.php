<!-- Ryan Bains-Jordan - Mlib -->

<!DOCTYPE html>
<?php
	session_start();
	include 'mlib_values.php';
	include 'mlib_functions.php';
	include 'mlib_header.php';
	include 'mlib_sidebar.php';
	include 'mlib_footer.php';
?>

<html>
<?php get_meta(); ?>
<body>
	<div class="container">
		<?php 
		get_header($_GLOBAL['header']);
		get_navbar($_GLOBAL['main_nav'], "Media Status");
		?>
		<div class="row justify-content-sm-center">
			<section class="module col-sm-12 col-lg-8">
				
				<?php
				
				// Database Connection
				$db = db_connection();
				
				// Display everything in mlib_media table
				echo "<h3>All Media</h3>";
				?>
				
				<table class="table table-overflow">
					<thead>
						<tr>
							<th>Title</th>
							<th>Author</th>
							<th>Description</th>
							<th>Type</th>
							<th>Reserved By</th>
							<th>Due By</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$sql = "SELECT * FROM media WHERE status = 'active'";
						$results = $db->query($sql);
						foreach ( $results as $row ) {
							echo '<tr>
								<td>' . $row['title'] . '</td>
								<td>' . $row['author'] . '</td>
								<td>' . $row['description'] . '</td>
								<td>' . $row['type'] . '</td>';
							$user_id = $row['user_id'];
							if ( $user_id > 0 ) {
								$sql = "SELECT * FROM mlib_users WHERE id = '$user_id'";
								$results = $db->query($sql)->fetch();
								$user_name = $results['first'] . " " . $results['last'];
								$date_in = $row['date_in'];
							} else {
								$user_name = "Available";
								$date_in = "Not Reserved";
							}
							echo '<td>' . $user_name . '</td>
								<td>' . $date_in . '</td>
								</tr>';
						}	
						?>
					</tbody>
				</table>
				
				<?php
				$db = null;
				
				?>
					
			</section>
		</div>
	</div>
	<?php get_footer() ?>
</body>
</html>