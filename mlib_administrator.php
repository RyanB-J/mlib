
<!-- Ryan Bains-Jordan - Mlib -->

<!DOCTYPE html>
<?php
	session_start();
	include 'mlib_values.php';
	include 'mlib_functions.php';
	include 'mlib_header.php';
	include 'mlib_sidebar.php';
	include 'mlib_footer.php';
?>

<html>
<?php get_meta(); ?>
<body>
	<div class="container">
		<?php 
		get_header($_GLOBAL['header']);
		get_navbar($_GLOBAL['main_nav'], "Admin");
		?>
		<div class="row justify-content-sm-center">
			<section class="module col-sm-12 col-lg-8">
				<?php
				// Assign variables to form data
				if ( isset( $_SESSION['valid_user'] ) ) {
					$is_admin = true;
				}
				if ( isset( $_POST['id'] ) ) {
					$id = $_POST['id'];
				}
				if ( isset( $_POST['login'] ) ) {
					$login = trim( $_POST['login'] );
				}
				if ( isset( $_POST['password'] ) ) {
					$password = trim( $_POST['password'] );
				}
				if ( isset( $_POST['submit'] ) ) {
					$submit = $_POST['submit'];
				}	
				
				// Database Connection
				$db = db_connection();
				
				/********** If form has not been submitted **********/	
				if ( ! isset( $submit ) and $is_admin ):
				?>
				<h3>Change Administrator Privileges</h3>
				<form action="mlib_administrator.php" method="post">
					<table class="table">
						<thead>
							<tr>
								<th class="check-col"></th>
								<th>User</th>
								<th>Username</th>
							</tr>
						</thead>
						<tbody>
							<?php
							// Create a list of all users
							$result = $db->query("SELECT * FROM mlib_users");
							foreach ($result as $row) {
								echo '<tr>';
								echo '<td><input type="radio" class="form-control" name="id" value="'.$row['id'].'"></td>';
								echo '<td>'.$row['first'].' '.$row['last'].'</td>';
								echo '<td>'.$row['login'].'</td>';
								echo '</tr>';
							}
							?>
						</tbody>
					</table>
					<hr>
					
					<p>Clicking an entry with a login will remove administration privileges.</p>
					<p>Clicking an entry without a login will enable administration privileges. Enter login and password below:</p>
					
					<table class="table">
						<tbody>
							<tr>
								<td>Username</td>
								<td><input class="form-control" type="text" name="login" maxlength="50" value="<?php echo $login; ?>"></td>
							</tr>
							<tr>
								<td>Password</td>
								<td><input class="form-control" type="password" name="password" maxlength="50" value="<?php echo $password; ?>"></td>
							</tr>
						</tbody>
					</table>
					<input type="submit" class="btn btn-outline-secondary" name="submit" value="Submit">
				</form>
						
				<?php
				$db = null;
				
				/********** If form has been submitted with no selected user **********/
				elseif ( empty( $id ) and $is_admin ):
				?>
				<h3>Change Administrator Privileges</h3>
				<form action="mlib_administrator.php" method="post">
					<table class="table">
						<thead>
							<tr>
								<th class="check-col"></th>
								<th>User</th>
								<th>Username</th>
							</tr>
						</thead>
						<tbody>
							<?php
							// Create a list of all users
							$result = $db->query("SELECT * FROM mlib_users");
							foreach ($result as $row) {
								echo '<tr>';
								echo '<td><input type="radio" class="form-control" name="id" value="'.$row['id'].'"></td>';
								echo '<td>'.$row['first'].' '.$row['last'].'</td>';
								echo '<td>'.$row['login'].'</td>';
								echo '</tr>';
							}
							if ( empty( $id ) ) {
								echo '<tr><td colspan="3"><div class="alert alert-danger">Please select a user.</div></td></tr>';
							}
							?>
						</tbody>
					</table>
					<hr>
					
					<p>Clicking an entry with a login will remove administration privileges.</p>
					<p>Clicking an entry without a login will enable administration privileges. Enter login and password below:</p>
					
					<table class="table">
						<tbody>
							<tr>
								<td>Username</td>
								<td><input class="form-control" type="text" name="login" maxlength="50" value="<?php echo $login; ?>"></td>
							</tr>
							<tr>
								<td>Password</td>
								<td><input class="form-control" type="password" name="password" maxlength="50" value="<?php echo $password; ?>"></td>
							</tr>
						</tbody>
					</table>
					<input type="submit" class="btn btn-outline-secondary" name="submit" value="Submit">
				</form>
						
				<?php
				$db = null;

				/********** If form has been submitted with a selected user **********/
				elseif ( $is_admin ):
				
					$sql = "SELECT * FROM mlib_users WHERE id = $id";
					$result = $db->query($sql)->fetch(); 
					
					/***** If login and password fields are populated on the selected user *****/
					if ( ! empty( $result['login'] ) and ! empty( $result['password'] ) ) {
						
						// Wipe out login and password info
						$db->exec("UPDATE mlib_users SET login = '' WHERE id = $id");
						$db->exec("UPDATE mlib_users SET password = '' WHERE id = $id");
						
						?>
						<h3>Change Administrator Privileges</h3>
						<div class="alert alert-success">Users updated.</div>
						<form action="mlib_administrator.php" method="post">
							<table class="table">
								<thead>
									<tr>
										<th class="check-col"></th>
										<th>User</th>
										<th>Username</th>
									</tr>
								</thead>
								<tbody>
									<?php
									// Create a list of all users
									$result = $db->query("SELECT * FROM mlib_users");
									foreach ($result as $row) {
										echo '<tr>';
										echo '<td><input type="radio" class="form-control" name="id" value="'.$row['id'].'"></td>';
										echo '<td>'.$row['first'].' '.$row['last'].'</td>';
										echo '<td>'.$row['login'].'</td>';
										echo '</tr>';
									}
									?>
								</tbody>
							</table>
							<hr>
							
							<p>Clicking an entry with a login will remove administration privileges.</p>
							<p>Clicking an entry without a login will enable administration privileges. Enter login and password below:</p>
							
							<table class="table">
								<tbody>
									<tr>
										<td>Username</td>
										<td><input class="form-control" type="text" name="login" maxlength="50"></td>
									</tr>
									<tr>
										<td>Password</td>
										<td><input class="form-control" type="password" name="password" maxlength="50"></td>
									</tr>
								</tbody>
							</table>
							<input type="submit" class="btn btn-outline-secondary" name="submit" value="Submit">
						</form>
						<?php
					}
					
					/***** If login data (from database) is empty for the selected user *****/
					else {
						
						/* If login or password fields are empty OR if password is less than 8 characters */
						if ( empty( $login ) || empty( $password ) or strlen( $password ) < 8 ) {
							
							?>
							<h3>Change Administrator Privileges</h3>
							<form action="mlib_administrator.php" method="post">
								<table class="table">
									<thead>
										<tr>
											<th class="check-col"></th>
											<th>User</th>
											<th>Username</th>
										</tr>
									</thead>
									<tbody>
										<?php
										// Create a list of all users
										$result = $db->query("SELECT * FROM mlib_users");
										foreach ($result as $row) {
											echo '<tr>';
											echo '<td><input type="radio" class="form-control" name="id" value="'.$row['id'].'"'.($row['id']==$id?' checked':'').'></td>';
											echo '<td>'.$row['first'].' '.$row['last'].'</td>';
											echo '<td>'.$row['login'].'</td>';
											echo '</tr>';
										}
										?>
									</tbody>
								</table>
								<hr>
								
								<p>Clicking an entry with a login will remove administration privileges.</p>
								<p>Clicking an entry without a login will enable administration privileges. Enter login and password below:</p>
								
								<table class="table">
									<tbody>
										<tr>
											<td>Username</td>
											<td>
												<input class="form-control" type="text" name="login" maxlength="50" value="<?php echo $login; ?>">
												<?php
												if ( empty( $login ) ) {
													echo '<div class="alert alert-danger">Please enter a new username.</div>';
												}
												?>
											</td>
										</tr>
										<tr>
											<td>Password</td>
											<td>
												<input class="form-control" type="password" name="password" maxlength="50" value="<?php echo $password; ?>">
												<?php
												if ( empty( $password ) ) {
													echo '<div class="alert alert-danger">Please enter a new password.</div>';
												} elseif ( strlen( $password ) < 8 ) {
													echo '<div class="alert alert-danger">Password must be 8 or more characters.</div>';
												}
												?>
											</td>
										</tr>
									</tbody>
								</table>
								<input type="submit" class="btn btn-outline-secondary" name="submit" value="Submit">
							</form>
							<?php
						}
						
						/* If there are no errors */
						else {
							
							// Hash the password
							$password = sha1( $password );
							
							// Update user data with the new data
							$db->exec("UPDATE mlib_users SET login = '$login' WHERE id = $id");
							$db->exec("UPDATE mlib_users SET `password` = '$password' WHERE id = $id");
							
							?>
							<h3>Change Administrator Privileges</h3>
							<div class="alert alert-success">Users updated.</div>
							<form action="mlib_administrator.php" method="post">
								<table class="table">
									<thead>
										<tr>
											<th class="check-col"></th>
											<th>User</th>
											<th>Username</th>
										</tr>
									</thead>
									<tbody>
										<?php
										// Create a list of all users
										$result = $db->query("SELECT * FROM mlib_users");
										foreach ($result as $row) {
											echo '<tr>';
											echo '<td><input type="radio" class="form-control" name="id" value="'.$row['id'].'"></td>';
											echo '<td>'.$row['first'].' '.$row['last'].'</td>';
											echo '<td>'.$row['login'].'</td>';
											echo '</tr>';
										}
										?>
									</tbody>
								</table>
								<hr>
								
								<p>Clicking an entry with a login will remove administration privileges.</p>
								<p>Clicking an entry without a login will enable administration privileges. Enter login and password below:</p>
								
								<table class="table">
									<tbody>
										<tr>
											<td>Username</td>
											<td><input class="form-control" type="text" name="login" maxlength="50"></td>
										</tr>
										<tr>
											<td>Password</td>
											<td><input class="form-control" type="password" name="password" maxlength="50"></td>
										</tr>
									</tbody>
								</table>
								<input type="submit" class="btn btn-outline-secondary" name="submit" value="Submit">
							</form>
							<?php
						}
					}
										
				$db = null;
				
				/***** If user is not admin *****/
				else:
				
				we_are_not_admin();
				$db = null;
				
				endif;
												
				?>			
			</section>
		</div>
	</div>
	<?php get_footer() ?>
</body>
</html>