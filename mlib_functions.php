<?php
/*
* MLib Functions
* Global convience functions
*/

function db_connection() {
	// Flag to keep track of which connection method works
	$flag = false;
	
	try {
		// Database Connection with unique port
		$db = new PDO( "mysql:host=".DB_SERVER.";dbname=".DB_DATABASE.";port=".DB_PORT, DB_USER, DB_PASS );
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$flag = true;
		
	} catch ( PDOException $e ) {
		$db = null;
	}
			
	if ( ! $flag ) {
		try {
			// Prof. McGowen's Database Connection
			$db = new PDO( DB_PATH, DB_LOGIN, DB_PW );
			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		
		} catch ( PDOException $e ) {
			// Connection Failure
			echo '<div class="alert alert-danger">Connection Failed. '. $e->getMessage() .'</div>';
			$db = null;	
		}		
	}
	return $db;
}

// Convert any date into the correct date format for storing
function convert_date( $date ) {
	return date('Y-m-d',strtotime($date));
}

// Check the date to ensure it isn't empty or in the past
function check_date( $date ) {
	
	// If date is empty
	if (empty($date)) {
		return 'Please enter a date.';
	
	} else {
		
		// Todays Date
		$today = date('Y-m-d');
		
		// Compare Date to Today
		if($date < $today) {
			return 'Please enter a valid future date.';
		}
	}
	
	return null;
}

function we_are_not_admin() {
	?>
	<h3>Warning</h3>
	<div class="alert alert-warning">This page is restricted to administrators only.</div>
	<?php
}
?>