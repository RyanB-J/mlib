<?php
/*
* MLib Sidebar
* Navigation Menu Constructor
*/

// Session
session_start();

// Main Nav Bar
$_GLOBAL['main_nav'] = array(
	'Media Status' => 'mlib_status.php',
	'Reserve Media' => 'mlib_reserve.php',
	'Release Media' => 'mlib_release.php',	
);
if ( isset( $_SESSION['valid_user'] ) ) {
	$admin_menu = array(
		'Admin' => array(
			'Add Media' => 'mlib_media.php',
			'Upload Media' => 'mlib_upload.php',
			'Add User' => 'mlib_users.php',
			'Admin Config' => 'mlib_administrator.php'
		),
		'Logout' => 'mlib_logout.php',	
	);
	$_GLOBAL['main_nav'] = array_merge( $_GLOBAL['main_nav'], $admin_menu );
} else {	
	$_GLOBAL['main_nav'] = array_merge( $_GLOBAL['main_nav'], array('Login' => 'mlib_login.php') );
}
	
function get_navbar($nav_array, $nav_active, $nav_brand) {
	?>
	<div class="row justify-content-sm-center">
		<section class="menu col-sm-12 col-lg-8 bg-light">
			<nav class="navbar navbar-expand-sm navbar-light bg-light">
				<?php 
				if (!is_null($nav_brand)) {
					echo '<a class="navbar-brand" href="#">' . $nav_brand . '</a>';
				}
				?>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
			
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav">
						<?php
						if (!is_null($nav_array)) {
							foreach ($nav_array as $nav_name => $nav_href) {
								
								// Add the list item for either dropdown or regular links
								if ($nav_name == $nav_active) {
										echo '<li class="nav-item'.(is_array($nav_href) ? ' dropdown' : '').' active">';
									} else {
										echo '<li class="nav-item'.(is_array($nav_href) ? ' dropdown' : '').'">';
								}
								
								// If link is an array (dropdown)
								if (is_array($nav_href)) {
									?>
									<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<?php echo $nav_name; ?>
									</a>
									<div class="dropdown-menu" aria-labelledby="navbarDropdown">
										<?php
										foreach ($nav_href as $drop_name => $drop_href) {
											echo '<a class="dropdown-item" href="' . $drop_href . '">' . $drop_name;
											if ($nav_name == $nav_active) {
												echo '<span class="sr-only">(current)</span>';
											}
											echo '</a>';
										}
										?>
									</div>
								<?php
								}
								
								// If link is a normal link
								else {
									?>
									<a class="nav-link" href="<?php echo $nav_href; ?>">
										<?php 
										echo $nav_name;
										if ($nav_name == $nav_active) {
											echo '<span class="sr-only">(current)</span>';
										}
										?>
									</a>
								<?php
								}
								echo '</li>';
							}
						}	
						?>
					</ul>
				</div>
			</nav>		
		</section>
	</div>
	<?php
}
?>